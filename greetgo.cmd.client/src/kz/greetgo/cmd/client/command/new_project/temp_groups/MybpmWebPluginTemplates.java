package kz.greetgo.cmd.client.command.new_project.temp_groups;

import java.util.function.Consumer;
import java.util.function.Supplier;
import kz.greetgo.cmd.client.command.new_project.ProjectTemplateGroup;
import kz.greetgo.cmd.client.command.options.CmdOptions;
import kz.greetgo.cmd.core.copier.NameStyled;
import kz.greetgo.cmd.core.copier.TemplateCopier;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class MybpmWebPluginTemplates implements ProjectTemplateGroup {
  @Override
  public String name() {
    return "mybpm-web-plugin-templates";
  }

  @Override
  public String description() {
    return "MyBPM Web plugins templates";
  }

  @Override
  public String gitUrl() {
    return "https://gitlab.com/greetgo/mybpm-web-plugin-template.git";
  }

  private final Supplier<NameStyled> projectNameArg;
  private final CmdOptions           options = new CmdOptions();


  @Override
  public String projectName() {
    return NameStyled.parse("mybpm-web-plugin").concat(projectNameArg.get()).dashed();
  }

  @Override
  public CmdOptions options() {
    return options;
  }

  @Override
  public Consumer<TemplateCopier> templateCopierPreparation() {
    return templateCopier -> {
      templateCopier.setVariable("PROJECT_NAME",
                                 NameStyled.parse("mybpm-web-plugin").concat(projectNameArg.get()));
      templateCopier.setVariable("PLUGIN_NAME", projectNameArg.get());

    };
  }
}
