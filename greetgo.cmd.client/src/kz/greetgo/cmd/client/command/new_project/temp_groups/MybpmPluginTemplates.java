package kz.greetgo.cmd.client.command.new_project.temp_groups;

import java.util.function.Consumer;
import java.util.function.Supplier;
import kz.greetgo.cmd.client.command.new_project.ProjectTemplateGroup;
import kz.greetgo.cmd.client.command.options.CmdOptions;
import kz.greetgo.cmd.core.copier.NameStyled;
import kz.greetgo.cmd.core.copier.TemplateCopier;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class MybpmPluginTemplates implements ProjectTemplateGroup {
  @Override
  public String name() {
    return "mybpm-plugin-templates";
  }

  @Override
  public String description() {
    return "MyBPM plugins templates";
  }

  @Override
  public String gitUrl() {
    return "https://gitlab.com/greetgo/mybpm-plugin-template.git";
  }

  private final Supplier<NameStyled> projectNameArg;
  private final CmdOptions           options = new CmdOptions();

  {
    options.addInt("port-base").mandatory(true)
           .shortDescription("initial port value in the range of ports")
           .fullDescription("Specifies the initial port value in the range of ports" +
                              " that will be used in the project");
  }

  @Override
  public String projectName() {
    return NameStyled.parse("mybpm-plugin").concat(projectNameArg.get()).dashed();
  }

  @Override
  public CmdOptions options() {
    return options;
  }

  @Override
  public Consumer<TemplateCopier> templateCopierPreparation() {
    return templateCopier -> {
      templateCopier.setVariable("PROJECT_NAME",
                                 NameStyled.parse("mybpm-plugin").concat(projectNameArg.get()));
      templateCopier.setVariable("PLUGIN_NAME", projectNameArg.get());

      templateCopier.setPortBase(options.getInt("port-base").orElseThrow());
    };
  }
}
