package kz.greetgo.cmd.client.command.new_project.temp_groups;

import java.util.function.Consumer;
import java.util.function.Supplier;
import kz.greetgo.cmd.client.command.new_project.ProjectTemplateGroup;
import kz.greetgo.cmd.client.command.options.CmdOptions;
import kz.greetgo.cmd.core.copier.NameStyled;
import kz.greetgo.cmd.core.copier.TemplateCopier;
import kz.greetgo.cmd.core.util.StrUtil;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GreetgoTemplates implements ProjectTemplateGroup {

  private final Supplier<NameStyled> projectNameArg;
  private final CmdOptions           options = new CmdOptions();

  @Override
  public String name() {
    return "greetgo.templates";
  }

  @Override
  public String description() {
    return "Common greetgo project templates";
  }

  @Override
  public String gitUrl() {
    return "https://gitlab.com/greetgo/greetgo-templates.git";
  }

  @Override
  public String projectName() {
    return projectNameArg.get().dashed();
  }

  @Override
  public CmdOptions options() {
    return options;
  }

  @Override
  public Consumer<TemplateCopier> templateCopierPreparation() {
    return templateCopier -> {
      templateCopier.setVariable("PROJECT_NAME", projectNameArg.get());
      templateCopier.setVariable("RND_SALT", StrUtil.generateSalt());
    };
  }
}
