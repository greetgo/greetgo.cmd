package kz.greetgo.cmd.client.command.new_project;

import java.util.function.Consumer;
import kz.greetgo.cmd.client.command.options.CmdOptions;
import kz.greetgo.cmd.core.copier.TemplateCopier;

public interface ProjectTemplateGroup {
  String name();

  String description();

  String projectName();

  String gitUrl();

  CmdOptions options();

  Consumer<TemplateCopier> templateCopierPreparation();
}
