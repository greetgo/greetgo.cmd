package kz.greetgo.cmd.client.command.new_project;

import java.io.File;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import kz.greetgo.cmd.client.command.new_project.temp_groups.GreetgoTemplates;
import kz.greetgo.cmd.client.command.new_project.temp_groups.MybpmPluginTemplates;
import kz.greetgo.cmd.client.command.new_project.temp_groups.MybpmWebPluginTemplates;
import kz.greetgo.cmd.client.command.new_sub.NewSubCommand;
import kz.greetgo.cmd.client.command.options.CmdOption;
import kz.greetgo.cmd.client.command.options.ParseOptionException;
import kz.greetgo.cmd.core.util.App;
import kz.greetgo.cmd.core.copier.NameStyled;
import kz.greetgo.cmd.core.copier.TemplateCopier;
import kz.greetgo.cmd.core.errors.SimpleExit;
import kz.greetgo.cmd.core.git.Git;
import kz.greetgo.cmd.core.util.Locations;
import kz.greetgo.cmd.core.util.StrUtil;
import lombok.NonNull;

import static java.util.stream.Collectors.joining;

public class CommandNewProject extends NewSubCommand {

  private final LinkedHashMap<String, ProjectTemplateGroup> templateGroupMap = new LinkedHashMap<>();

  private NameStyled projectNameArg = NameStyled.parse("a-project-name");

  {
    addGroup(new GreetgoTemplates(() -> projectNameArg));
    addGroup(new MybpmPluginTemplates(() -> projectNameArg));
    addGroup(new MybpmWebPluginTemplates(() -> projectNameArg));
  }

  private String               templateGroup        = null;
  private String               templateName1        = null;
  private ProjectTemplateGroup projectTemplateGroup = null;

  @SuppressWarnings("SameParameterValue")
  private void addGroup(ProjectTemplateGroup group) {
    templateGroupMap.put(group.name(), group);
  }

  @Override
  public void exec(@NonNull List<String> subList) {

    if (subList.size() >= 1) {
      projectNameArg = NameStyled.parse(subList.get(0));
    }

    if (subList.size() < 2) {
      System.err.println("Incomplete command. Usage: \n");
      usage();
      throw new SimpleExit(1);
    }

    templateGroup = subList.get(1);

    if (!templateGroupMap.containsKey(templateGroup)) {
      unknownTemplateGroup();
      throw new SimpleExit(1);
    }

    projectTemplateGroup = templateGroupMap.get(templateGroup);

    if (projectTemplateGroup == null) {
      unknownTemplateGroup();
      throw new SimpleExit(1);
    }

    if (subList.size() == 2) {
      listTemplatesInGroup();
      throw new SimpleExit(1);
    }

    templateName1 = subList.get(2);

    try {
      projectTemplateGroup.options().parse(subList.subList(3, subList.size()));
    } catch (ParseOptionException e) {
      if (App.isDebug()) {
        e.printStackTrace();
      }
      System.err.println();
      System.err.println("17sgP7t3Qt :: Error in options: " + e.getMessage());
      System.err.println();
      restUsage();
      throw new SimpleExit(13);
    }

    if (projectTemplateGroup.options().hasGo()) {
      projectTemplateGroup.options().validateMandatoryOptions();
      executeCommand();
      return;
    }

    restUsage();

    throw new SimpleExit(1);
  }

  private void unknownTemplateGroup() {
    System.err.println("Unknown template group `" + templateGroup + "'. Usage:\n");
    usage();
  }

  private void usage() {
    printUsage();
  }

  @Override
  public boolean accept(String strSubCommand) {
    return "project".equals(strSubCommand) || "p".equals(strSubCommand);
  }

  private static String toLenRight(String name, int length) {
    StringBuilder sb = new StringBuilder();
    sb.append(name);
    while (sb.length() < length) {
      sb.insert(0, ' ');
    }
    return sb.toString();
  }

  private void restUsage() {
    String cmd = cmdPrefix + " project " + projectNameArg.dashed() + ' '
      + templateGroup + ' ' + templateName1 + projectTemplateGroup.options().part() + ' ';

    int spaces = 20;

    System.err.println();

    for (final CmdOption<?> option : projectTemplateGroup.options().restOptions()) {
      System.err.println(cmd + "-" + option.name() + "=" + option.displayShortDescription());
      System.err.println(option.displayFullDescription(spaces));
    }

    System.err.println(cmd + "go");
    System.err.println(" ".repeat(spaces) + "To execute the command");
    System.err.println();
  }

  @Override
  public void printUsage() {
    System.err.println("  " + cmdPrefix + " [project | p] <ProjectName> <TemplateGroup> <TemplateName> [options] go");
    System.err.println("      ");
    System.err.println("      Creates new project with name <ProjectName> based on template <TemplateName>");
    System.err.println("      using options. Some template groups may have required options - this will be stated");
    System.err.println("      in the documentation for these template groups, if you do not specify `go` at the end");
    System.err.println("      of the command.");
    System.err.println("      ");
    System.err.println("      You can use the following template groups:");

    String prefixSpace = StrUtil.spaces(8);

    int    maxNameLength = templateGroupMap.values().stream().mapToInt(s -> s.name().length()).max().orElse(1);
    String nameHeader    = "<TemplateGroup>";
    if (maxNameLength < nameHeader.length()) {
      maxNameLength = nameHeader.length();
    }

    {
      System.err.println(prefixSpace);
      String name = prefixSpace + toLenRight(nameHeader, maxNameLength) + " - ";
      System.err.println(name + "<Description>");
    }

    for (ProjectTemplateGroup pt : templateGroupMap.values()) {

      String name  = prefixSpace + toLenRight(pt.name(), maxNameLength) + " - ";
      String space = StrUtil.spaces(name.length());
      String description = Arrays.stream(pt.description().split("\n"))
                                 .map(String::trim)
                                 .collect(joining("\n" + space));

      System.err.println(prefixSpace);
      System.err.println(name + description);
    }
    System.err.println(prefixSpace);

    System.err.println("      Use the following commands:");
    System.err.println();
    for (ProjectTemplateGroup pt : templateGroupMap.values()) {
      System.err.println(//"        " +
                         cmdPrefix + " project " + projectNameArg.dashed() + " " + pt.name());
    }
    System.err.println();
  }

  private void executeCommand() {
    Path gitPath = prepareTemplateGroup();

    Set<String> templateNameSet = Git.listRemoteBranches(gitPath)
                                     .stream()
                                     .map(this::extractTemplateName)
                                     .filter(Objects::nonNull)
                                     .collect(Collectors.toSet());

    if (!templateNameSet.contains(templateName1)) {
      System.err.println("Unknown variant `" + templateName1 + "'.");
      listTemplatesInGroup();
      throw new SimpleExit(1);
    }

    Git.checkout(gitPath, templateBranchName());

    String projectName = projectTemplateGroup.projectName();

    File projectDir = App.currentWorkingDir().resolve(projectName).toFile();

    if (projectDir.exists()) {
      System.err.println("Directory `" + projectName + "' already exists");
      throw new SimpleExit(1);
    }

    if (!projectDir.mkdir()) {
      System.err.println("Cannot create directory `" + projectName + "'");
      throw new SimpleExit(10);
    }

    TemplateCopier.of()
                  .from(gitPath)
                  .to(projectDir.toPath())
                  .use(projectTemplateGroup.templateCopierPreparation())
                  .copy();

    Git.init(projectDir.toPath());
    Git.addAll(projectDir.toPath());
    Git.commit(projectDir.toPath(), "Created with greetgo-cli by " + System.getProperty("user.name"));
  }

  private static Path templatesDir() {
    return Locations.localNewProject().resolve("templates-002");
  }

  private Path gitRepoPath(String templateName) {
    return templatesDir().resolve(templateName).resolve("git-repo");
  }

  private String extractTemplateName(String branchName) {
    if (!branchName.startsWith("t-")) {
      return null;
    }
    return branchName.substring(2);
  }

  private String templateBranchName() {
    return "t-" + templateName1;
  }

  private void listTemplatesInGroup() {
    System.err.println("You an use following templates:\n");
    String cmd = cmdPrefix + " project " + projectNameArg.dashed() + ' ' + templateGroup + ' ';
    System.err.println("list templates in group: " + templateGroup);
    System.err.println();

    Path gitPath = prepareTemplateGroup();

    Git.listRemoteBranches(gitPath)
       .stream()
       .map(this::extractTemplateName)
       .filter(Objects::nonNull)
       .forEachOrdered(tName -> System.err.println(cmd + tName));

    System.err.println();
  }

  private Path prepareTemplateGroup() {
    ProjectTemplateGroup projectTemplateGroup = templateGroupMap.get(templateGroup);
    if (projectTemplateGroup == null) {
      unknownTemplateGroup();
      throw new SimpleExit(1);
    }

    Path gitPath = gitRepoPath(templateGroup);

    if (gitPath.toFile().isDirectory()) {
      Git.fetchAllBranches(gitPath);
    } else {
      File parentFile = gitPath.toFile().getParentFile();
      parentFile.mkdirs();
      Git.gitClone(parentFile.toPath(), projectTemplateGroup.gitUrl(), gitPath.toFile().getName());
    }

    return gitPath;
  }
}
