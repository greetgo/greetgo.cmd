package kz.greetgo.cmd.client.command.options;

import java.util.Optional;

public class CmdOption_Bool extends CmdOption<CmdOption_Bool> {
  private Boolean defaultValue = false;

  public CmdOption_Bool(String name) {
    super(name);
  }

  public boolean defaultValue() {
    return defaultValue;
  }

  public CmdOption_Bool defaultValue(Boolean defaultValue) {
    this.defaultValue = defaultValue;
    return this;
  }

  @Override
  public void validateWriteValue(String optionValue, String optionEq) {
    throw new RuntimeException("7I09kVEhsC");
  }

  public Optional<Boolean> defaultBoolValue() {
    return Optional.ofNullable(defaultValue);
  }

  @Override
  public Optional<String> defaultStrValue() {
    return defaultBoolValue().map(String::valueOf);
  }

  @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
  public Optional<Boolean> toBool(Optional<String> strValue) {
    return strValue.map(this::toBool);
  }

  public boolean toBool(String strValue) {
    if (strValue == null) {
      return false;
    }
    switch (strValue.toLowerCase()) {
      case "true":
      case "t":
      case "1":
      case "on":
      case "yes":
      case "y":
      case "да":
      case "истина":
        return true;
      default:
        return false;
    }
  }

}
