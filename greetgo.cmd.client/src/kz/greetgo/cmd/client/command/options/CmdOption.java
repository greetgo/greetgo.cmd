package kz.greetgo.cmd.client.command.options;

import java.util.Optional;

public abstract class CmdOption<Super extends CmdOption<Super>> {
  protected final String name;
  protected       String shortDescription;
  protected       String fullDescription;

  public abstract void validateWriteValue(String optionValue, String optionEq);

  public abstract Optional<String> defaultStrValue();

  protected CmdOption(String name) {
    this.name        = name;
    shortDescription = name;
  }

  protected boolean mandatory = false;

  public String name() {
    return name;
  }

  public String shortDescription() {
    return shortDescription;
  }

  public Super shortDescription(String shortDescription) {
    this.shortDescription = shortDescription;
    return self();
  }

  public String fullDescription() {
    return fullDescription;
  }

  public Super fullDescription(String fullDescription) {
    this.fullDescription = fullDescription;
    return self();
  }

  private Super self() {
    //noinspection unchecked
    return (Super) this;
  }

  public boolean mandatory() {
    return mandatory;
  }

  public Super mandatory(boolean mandatory) {
    this.mandatory = mandatory;
    return self();
  }

  public String displayShortDescription() {
    return shortDescription.replaceAll("\\s+", "_");
  }

  public String displayFullDescription(int spaces) {

    StringBuilder sb = new StringBuilder();

    sb.append(" ".repeat(spaces)).append("-").append(name()).append("=").append(displayShortDescription()).append("\n");

    if (this.mandatory) {
      sb.append(" ".repeat(spaces)).append("( *Mandatory )\n");
    }

    var d = fullDescription;
    if (d == null) {
      return "";
    }

    for (final String s : d.split("\n")) {
      sb.append(" ".repeat(spaces)).append(s).append("\n");
    }

    return sb.toString();
  }
}
