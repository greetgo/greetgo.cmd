package kz.greetgo.cmd.client.command.options;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import static java.util.stream.Collectors.toList;

public class CmdOptions {

  private final List<CmdOption<?>> optionList = new ArrayList<>();
  private       boolean            hasGo;

  private final Map<String, List<String>> optionValues   = new HashMap<>();
  private final Set<String>               writtenOptions = new HashSet<>();

  public CmdOption_Str addStr(String key) {
    var ret = new CmdOption_Str(key);
    optionList.add(ret);
    return ret;
  }

  public CmdOption_Int addInt(String key) {
    var ret = new CmdOption_Int(key);
    optionList.add(ret);
    return ret;
  }

  public CmdOption_Bool addBool(String key) {
    var ret = new CmdOption_Bool(key);
    optionList.add(ret);
    return ret;
  }

  private Optional<CmdOption<?>> getOption(String optionName) {
    return optionList.stream().filter(o -> Objects.equals(optionName, o.name())).findAny();
  }

  public String part() {
    return optionsPart.isEmpty() ? "" : " " + String.join(" ", optionsPart);
  }

  private final List<String> optionsPart = new ArrayList<>();

  public void parse(List<String> optionCmdPart) {

    hasGo = false;
    if (optionCmdPart.size() >= 1) {
      hasGo = "go".equals(optionCmdPart.get(optionCmdPart.size() - 1));
    }

    for (int i = 0, n = optionCmdPart.size() - (hasGo ? 1 : 0); i < n; i++) {
      var optionEq = optionCmdPart.get(i);
      if (!optionEq.startsWith("-")) {
        throw new ParseOptionException("qNAyt2qC0V :: Option MUST starts with dash: " + optionEq);
      }
      var idx = optionEq.indexOf('=');
      if (idx < 0) {
        writeOption(optionEq.substring(1), null, optionEq);
      } else {
        writeOption(optionEq.substring(1, idx), optionEq.substring(idx + 1), optionEq);
      }
      optionsPart.add(optionEq);
    }

    if (hasGo) {
      validateMandatoryOptions();
    }

  }

  private void writeOption(String optionName, String optionValue, String optionEq) {

    for (final CmdOption<?> cmdOption : optionList) {
      if (Objects.equals(cmdOption.name(), optionName)) {
        writtenOptions.add(optionName);
        cmdOption.validateWriteValue(optionValue, optionEq);
        optionValues.computeIfAbsent(optionName, s -> new ArrayList<>())
                    .add(optionValue);
        return;
      }
    }

    throw new ParseOptionException("IPCpNt3wWD :: Unknown option: " + optionEq);
  }

  public List<CmdOption<?>> restOptions() {
    return optionList.stream()
                     .filter(o -> !writtenOptions.contains(o.name()))
                     .sorted(Comparator.comparing(CmdOption::name))
                     .collect(toList());
  }

  public Optional<String> getStr(String optionName) {
    var option = getOption(optionName).orElseThrow(() -> new RuntimeException("5VSc64bB8j :: No option " + optionName));

    return Optional.ofNullable(optionValues.get(optionName))
                   .stream()
                   .flatMap(Collection::stream)
                   .findFirst()
                   .or(option::defaultStrValue);
  }

  public Optional<Integer> getInt(String optionName) {

    var option = getOption(optionName).orElseThrow(() -> new RuntimeException("RoEM1X30dd :: No option " + optionName));

    if (!(option instanceof CmdOption_Int)) {
      throw new RuntimeException("bel09PL1XQ :: getInt can be called only for " + CmdOption_Int.class.getSimpleName());
    }

    var optionInt = (CmdOption_Int) option;

    return optionInt.toInt(

      Optional.ofNullable(optionValues.get(optionName))
              .stream()
              .flatMap(Collection::stream)
              .findFirst()

    ).or(optionInt::defaultIntValue);
  }


  public Optional<Boolean> getBool(String optionName) {
    var option = getOption(optionName).orElseThrow(() -> new RuntimeException("WsLY4Fn1gN :: No option " + optionName));

    if (!(option instanceof CmdOption_Bool)) {
      throw new RuntimeException("2Ot12SG9aF :: getBool can be called only for "
                                   + CmdOption_Bool.class.getSimpleName());
    }

    var optionBool = (CmdOption_Bool) option;

    return optionBool.toBool(

      Optional.ofNullable(optionValues.get(optionName))
              .stream()
              .flatMap(Collection::stream)
              .findFirst()

    ).or(optionBool::defaultBoolValue);
  }

  public boolean hasGo() {
    return hasGo;
  }

  public void validateMandatoryOptions() {
    for (final CmdOption<?> option : optionList) {
      if (option.mandatory() && !writtenOptions.contains(option.name())) {
        throw new ParseOptionException("J4vUIbUbNY :: Absent mandatory option: " +
                                         "-" + option.name() + "=" + option.displayShortDescription());
      }
    }
  }
}
