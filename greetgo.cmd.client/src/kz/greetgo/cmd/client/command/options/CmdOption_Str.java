package kz.greetgo.cmd.client.command.options;

import java.util.Optional;

public class CmdOption_Str extends CmdOption<CmdOption_Str> {
  private String defaultValue = "";

  public CmdOption_Str(String name) {
    super(name);
  }

  @Override
  public void validateWriteValue(String optionValue, String optionEq) {}

  @Override
  public Optional<String> defaultStrValue() {
    return Optional.ofNullable(defaultValue);
  }

  public String defaultValue() {
    return defaultValue;
  }

  public CmdOption_Str defaultValue(String defaultValue) {
    this.defaultValue = defaultValue;
    return this;
  }

}
