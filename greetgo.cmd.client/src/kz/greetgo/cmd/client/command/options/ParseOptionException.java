package kz.greetgo.cmd.client.command.options;

public class ParseOptionException extends RuntimeException {
  public ParseOptionException(String message, Exception e) {
    super(message, e);
  }

  public ParseOptionException(String message) {
    super(message);
  }
}
