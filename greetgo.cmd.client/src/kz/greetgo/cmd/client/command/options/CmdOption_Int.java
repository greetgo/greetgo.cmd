package kz.greetgo.cmd.client.command.options;

import java.util.Optional;

public class CmdOption_Int extends CmdOption<CmdOption_Int> {
  private int defaultValue = 0;

  public CmdOption_Int(String name) {
    super(name);
  }

  public int defaultValue() {
    return defaultValue;
  }

  public Optional<Integer> defaultIntValue() {
    return Optional.of(defaultValue);
  }

  @Override
  public Optional<String> defaultStrValue() {
    return defaultIntValue()
      .map(String::valueOf);
  }

  public CmdOption_Int defaultValue(int defaultValue) {
    this.defaultValue = defaultValue;
    return this;
  }

  @Override
  public void validateWriteValue(String optionValue, String optionEq) {
    try {
      toInt(optionValue);
    } catch (RuntimeException e) {
      throw new ParseOptionException("h1nymDh93J :: Illegal format of int argument: " + optionEq, e);
    }
  }

  @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
  public Optional<Integer> toInt(Optional<String> strVariant) {
    return strVariant.map(this::toInt);
  }

  public int toInt(String strVariant) {
    return Integer.parseInt(strVariant);
  }
}
