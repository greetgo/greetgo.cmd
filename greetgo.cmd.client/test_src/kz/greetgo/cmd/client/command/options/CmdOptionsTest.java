package kz.greetgo.cmd.client.command.options;

import java.util.List;
import org.fest.assertions.api.Assertions;
import org.testng.annotations.Test;

import static org.fest.assertions.api.Assertions.assertThat;

public class CmdOptionsTest {

  @Test
  public void parse__ok() {

    CmdOptions options = new CmdOptions();

    options.addStr("test1");
    options.addInt("test2");

    //
    //
    options.parse(List.of("-test1=Hello", "-test2=345", "go"));
    //
    //

    assertThat(options.getStr("test1").orElseThrow()).isEqualTo("Hello");
    assertThat(options.getInt("test2").orElseThrow()).isEqualTo(345);
    assertThat(options.hasGo()).isTrue();
  }

  @Test
  public void parse__ok__defaultValue() {

    CmdOptions options = new CmdOptions();

    options.addStr("test1").defaultValue("Hello test1").shortDescription("test 1");
    options.addInt("test2").defaultValue(1238).shortDescription("test 2");
    options.addBool("test3").defaultValue(true).shortDescription("test 3");

    //
    //
    options.parse(List.of());
    //
    //

    assertThat(options.getStr("test1").orElseThrow()).isEqualTo("Hello test1");
    assertThat(options.getInt("test2").orElseThrow()).isEqualTo(1238);
    assertThat(options.getBool("test3").orElseThrow()).isEqualTo(true);
    assertThat(options.hasGo()).isFalse();
  }

  @Test
  public void parse__boolDefaultNull() {

    CmdOptions options = new CmdOptions();

    options.addBool("test1").defaultValue(null).shortDescription("test 1");
    options.addBool("test2").defaultValue(null).shortDescription("test 2");
    options.addBool("test3").defaultValue(null).shortDescription("test 3");

    //
    //
    options.parse(List.of());
    //
    //

    assertThat(options.getBool("test1").orElse(null)).isNull();
    assertThat(options.getBool("test2").orElse(true)).isTrue();
    assertThat(options.getBool("test3").orElse(false)).isFalse();
    assertThat(options.hasGo()).isFalse();
  }

  @Test
  public void parse__illegalIntFormat() {

    CmdOptions options = new CmdOptions();

    options.addStr("test1");
    options.addInt("test2");

    ParseOptionException err = null;

    try {
      //
      //
      options.parse(List.of("-test1=Hello", "-test2=aa345", "go"));
      //
      //
      Assertions.fail("KW5pZqa7bx :: Cannot be here: method must throw an error");
    } catch (ParseOptionException e) {
      err = e;
    }

    assertThat(err).isNotNull();
    assertThat(err.getMessage()).endsWith("Illegal format of int argument: -test2=aa345");
  }

  @Test
  public void parse__illegalIntFormat_null() {

    CmdOptions options = new CmdOptions();

    options.addStr("test1");
    options.addInt("test2");

    ParseOptionException err = null;

    try {
      //
      //
      options.parse(List.of("-test1=Hello", "-test2", "go"));
      //
      //
      Assertions.fail("KW5pZqa7bx :: Cannot be here: method must throw an error");
    } catch (ParseOptionException e) {
      err = e;
    }

    assertThat(err).isNotNull();
    assertThat(err.getMessage()).endsWith("Illegal format of int argument: -test2");
  }

  @Test
  public void parse__absentMandatory() {

    CmdOptions options = new CmdOptions();

    options.addStr("test1").mandatory(true).shortDescription("test1 desc");
    options.addInt("test2");

    ParseOptionException err = null;

    try {
      //
      //
      options.parse(List.of("-test2=345"));
      //
      //
      Assertions.fail("3cKV5fwHXY :: Cannot be here: method must throw an error");
    } catch (ParseOptionException e) {
      err = e;
    }

    assertThat(err).isNotNull();
    assertThat(err.getMessage()).endsWith("Absent mandatory option: -test1=<test1 desc>");
  }

  @Test
  public void parse__optionMustStartsWithDash() {

    CmdOptions options = new CmdOptions();

    options.addStr("test1").shortDescription("test1 desc");
    options.addInt("test2");

    ParseOptionException err = null;

    try {
      //
      //
      options.parse(List.of("test2=aa345"));
      //
      //
      Assertions.fail("L9mLVwZ95i :: Cannot be here: method must throw an error");
    } catch (ParseOptionException e) {
      err = e;
    }

    assertThat(err).isNotNull();
    assertThat(err.getMessage()).endsWith("Option MUST starts with dash: test2=aa345");
  }

  @Test
  public void parse__unknownOption() {

    CmdOptions options = new CmdOptions();

    options.addStr("test1a").shortDescription("test1 desc");
    options.addInt("test2a");

    ParseOptionException err = null;

    try {
      //
      //
      options.parse(List.of("-test2=aa345"));
      //
      //
      Assertions.fail("L9mLVwZ95i :: Cannot be here: method must throw an error");
    } catch (ParseOptionException e) {
      err = e;
    }

    assertThat(err).isNotNull();
    assertThat(err.getMessage()).endsWith("Unknown option: -test2=aa345");
  }
}
