#!/usr/bin/env bash
set -e
cd "$(dirname "$0")" || exit 43

if ! which curl >/dev/null
then
  echo curl does not install >&2
  echo Please install curl >&2
  echo You can install curl with command: >&2
  echo "    " >&2
  echo "    sudo apt-get install curl" >&2
  echo "    " >&2
  exit 131
fi

if [ "$GREETGO_LOCAL_DIR" = "" ]; then
  GREETGO_LOCAL_DIR="$HOME/.local/greetgo"
fi

export JAVA_HOME="$GREETGO_LOCAL_DIR/jdk17"

if [ ! -d "$JAVA_HOME" ]
then
  download_java17_link="https://download.oracle.com/java/17/latest/jdk-17_linux-x64_bin.tar.gz"
  java17_archive_download="$GREETGO_LOCAL_DIR/jdk-17_linux-x64_bin.tar.gz-download"
  java17_archive="$GREETGO_LOCAL_DIR/jdk-17_linux-x64_bin.tar.gz"

  if [ ! -f "$java17_archive" ] ; then
    mkdir -p "$(dirname "$java17_archive_download")"
    echo "#"
    echo "# Downloading $download_java17_link ..."
    echo "#"
    if ! curl -L -o "$java17_archive_download" -C - "$download_java17_link"; then
      rm -rf "$JAVA_HOME" "$java17_archive_download" "$java17_archive"
      echo "i cannot DOWNLOAD file $download_java17_link" >&2
      exit 132
    fi

    mv "$java17_archive_download" "$java17_archive"
  fi

  if ! tar xf "$java17_archive" -C "$GREETGO_LOCAL_DIR"
  then
    echo "i cannot extract tar archive $java17_archive" >&2
    rm -f "$java17_archive"
    echo "Please run command again" >&2
    exit 133
  fi

  mkdir -p "$GREETGO_LOCAL_DIR"
  find "$GREETGO_LOCAL_DIR" -maxdepth 1 -type d -name "jdk-17*" -exec ln -s {} "$JAVA_HOME" \;

fi

VERSION=$(cat "../version.txt")

CLIENT_JAR=greetgo.cmd.client/build/libs/greetgo.cmd.client-${VERSION}.jar

if [ "$*" == "up" ] ; then
  exec git pull
fi

REMOTE_VERSION=$(curl -s "https://gitlab.com/greetgo/greetgo.cmd/-/raw/master/version.txt?ref_type=heads")

if [ "$VERSION" != "$REMOTE_VERSION" ] ; then
  echo "LEFT VERSION: LOCAL VERSION $VERSION, REMOTE VERSION $REMOTE_VERSION " >&2
  git pull
fi

GRADLE="$(realpath ..)/gradlew"

if ! which java > /dev/null ; then
  echo java does not installed >&2
  echo Please install java 17+ >&2
  exit 134
fi

if [ "$*" == "status" ] ; then
  echo User "$USER"
  echo CURRENT_WORKING_DIR = "$CURRENT_WORKING_DIR"
  echo GRADLE = "$GRADLE"

  exit 0
fi

# shellcheck disable=SC2034
SCRIPTS_DIR=$PWD

cd ..

ROOT_DIR=$PWD

if [ ! -f "$CLIENT_JAR" ] || [ -n "$GREETGO_DEBUG" ] ; then
  cd greetgo.cmd.client || exit 75
  $GRADLE jar
  cd "$ROOT_DIR" || exit 76
  if ! [ -f "$CLIENT_JAR" ] ; then
    echo "Cannot build $CLIENT_JAR" >&2
    exit 1
  fi
fi

cd "$ROOT_DIR" || exit 65

# shellcheck disable=SC2086
# shellcheck disable=SC2048
exec java -jar "$CLIENT_JAR" $*
