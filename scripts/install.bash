#!/usr/bin/env bash

SHOW_TO_REBOOT=0

if [ ! -d "$HOME/bin/" ] ; then
  SHOW_TO_REBOOT=1
  mkdir "$HOME/bin/"
fi

curl https://gitlab.com/greetgo/greetgo.cmd/-/raw/master/scripts/greetgo.bash > "$HOME/bin/gg"

chmod +x "$HOME/bin/gg"

if [ "$SHOW_TO_REBOOT" = "1" ] ; then
  echo ""
  echo "##"
  echo "######  Installation complete. Please reboot the system and type gg in terminal  ######"
  echo "##"
  echo "## NOTE: Ensure directory $HOME/bin/ has been included into your environment variable PATH after reboot"
  echo "##"
  echo ""
  exit 0
fi

echo "Installation complete."

gg
