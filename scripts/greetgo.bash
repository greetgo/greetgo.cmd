#!/usr/bin/env bash

set -e

# shellcheck disable=SC2155
export USED_COMMAND="$(basename "$0")"

export CURRENT_WORKING_DIR=$PWD

if [ -f "$HOME/IdeaProjects/greetgo.cmd/scripts/cmd.bash" ]; then
  cd "$HOME/IdeaProjects/greetgo.cmd/scripts" || exit 178
  export GREETGO_DEBUG=1
  exec bash cmd.bash "$*"
fi

if ! which git >/dev/null; then
  echo git does not installed >&2
  echo Please install git >&2
  echo You can install git with command: >&2
  echo "    " >&2
  echo "    sudo apt-get install git" >&2
  echo "    " >&2
  exit 121
fi

if ! which curl >/dev/null; then
  echo curl does not install >&2
  echo Please install curl >&2
  echo You can install curl with command: >&2
  echo "    " >&2
  echo "    sudo apt-get install curl" >&2
  echo "    " >&2
  exit 122
fi

export GREETGO_LOCAL_DIR="$HOME/.local/greetgo"

REPO_DIR="$GREETGO_LOCAL_DIR/repo"

if [ ! -d "$REPO_DIR/greetgo.cmd" ]; then
  mkdir -p "$REPO_DIR"
  cd "$REPO_DIR" || exit 201
  if ! git clone https://gitlab.com/greetgo/greetgo.cmd.git greetgo.cmd
  then
    exit 123
  fi
fi

cd "$REPO_DIR/greetgo.cmd/scripts" || exit 231

exec bash cmd.bash "$*"
