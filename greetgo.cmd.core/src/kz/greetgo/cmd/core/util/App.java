package kz.greetgo.cmd.core.util;

import java.nio.file.Path;
import java.nio.file.Paths;

import static java.util.Objects.requireNonNull;

public class App {
  public static boolean isDebug() {
    return "1".equals(System.getenv("GREETGO_DEBUG"));
  }

  public static String usedCommand() {
    return System.getenv("USED_COMMAND");
  }

  public static Path currentWorkingDir() {
    return Paths.get(requireNonNull(System.getenv("CURRENT_WORKING_DIR"),
                                    "rhVhNN0NOu :: env CURRENT_WORKING_DIR"));
  }

  public static String version() {
    return App.class.getPackage().getSpecificationVersion();
  }

  public static String gitId() {
    return App.class.getPackage().getImplementationVersion();
  }
}
