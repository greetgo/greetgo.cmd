package kz.greetgo.cmd.core.util;

import java.util.Random;

import static java.lang.Character.isLowerCase;
import static java.lang.Character.isUpperCase;
import static java.lang.Character.toLowerCase;
import static java.lang.Character.toUpperCase;

public class RndUtil {

  private static final char[] CHARS;

  static {
    final int letterCount = ('z' - 'a') + 1;
    CHARS = new char[letterCount * 2 + 10];
    for (int i = 0; i < 10; i++) {
      CHARS[i] = (char) ('0' + i);
    }
    for (int i = 0; i < letterCount; i++) {
      CHARS[2 * i + 10] = (char) ('A' + i);
      CHARS[2 * i + 11] = (char) ('a' + i);
    }
  }

  public static String rndStr(int length, Random rnd) {
    char[] chars       = new char[length];
    var    charsLength = CHARS.length;
    for (int i = 0; i < length; i++) {
      chars[i] = CHARS[rnd.nextInt(charsLength)];
    }

    killGreenUnder(chars);

    return new String(chars);
  }

  public static void killGreenUnder(char[] chars) {
    int length = chars.length;

    while (true) {
      boolean rete = true;

      for (int i = 0; i < length - 3; i++) {
        char c1 = chars[i + 0];
        char c2 = chars[i + 1];
        char c3 = chars[i + 2];
        char c4 = chars[i + 3];
        if (isUpperCase(c1) && isLowerCase(c2) && isLowerCase(c3) && isLowerCase(c4)) {
          chars[i + 3] = toUpperCase(c4);
          rete         = false;
        }
      }

      for (int i = 0; i < length - 3; i++) {
        char c1 = chars[i + 0];
        char c2 = chars[i + 1];
        char c3 = chars[i + 2];
        char c4 = chars[i + 3];
        if (isUpperCase(c1) && isUpperCase(c2) && isUpperCase(c3) && isUpperCase(c4)) {
          chars[i + 3] = toLowerCase(c4);
          rete         = false;
        }
      }

      for (int i = 0; i < length - 3; i++) {
        char c1 = chars[i + 0];
        char c2 = chars[i + 1];
        char c3 = chars[i + 2];
        char c4 = chars[i + 3];
        if (isLowerCase(c1) && isLowerCase(c2) && isLowerCase(c3) && isLowerCase(c4)) {
          chars[i + 3] = toUpperCase(c4);
          rete         = false;
        }
      }

      if (rete) {
        return;
      }
    }
  }

}
