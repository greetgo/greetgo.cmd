package kz.greetgo.cmd.core.copier;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.lang.Character.isAlphabetic;
import static java.lang.Character.isDigit;
import static java.lang.Character.isUpperCase;
import static java.lang.Character.toLowerCase;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toUnmodifiableList;

public class NameStyled {

  public final List<String> words;

  private NameStyled(List<String> words) {
    this.words = words;
  }

  public static NameStyled parse(String input) {

    if (input == null) {
      return new NameStyled(List.of());
    }

    {
      boolean[] leftCut = new boolean[input.length()];

      for (int i = 1, n = input.length(); i < n; i++) {
        leftCut[i] = isUpperCase(input.charAt(i));
      }
      for (int i = 1, n = input.length() - 1; i < n; i++) {
        if (isUpperCase(input.charAt(i - 1)) && isUpperCase(input.charAt(i)) && isUpperCase(input.charAt(i + 1))) {
          leftCut[i] = false;
        }
      }

      for (int i = 0, n = input.length(), last = n - 1; i < n; i++) {
        if (Character.isDigit(input.charAt(i))) {
          leftCut[i] = false;
          if (i != last) {
            leftCut[i + 1] = true;
          }
        }
      }

      for (int i = 0, n = input.length() - 1; i < n; i++) {
        char c1 = input.charAt(i);
        char c2 = input.charAt(i + 1);
        if (isUpperCase(c1) && !isAlphabetic(c2)) {
          leftCut[i] = false;
        }
      }

      if (input.length() >= 2) {
        char c1 = input.charAt(input.length() - 2);
        char c2 = input.charAt(input.length() - 1);
        if (isUpperCase(c1) && isUpperCase(c2)) {
          leftCut[input.length() - 1] = false;
        }
      }

      List<String> words = new ArrayList<>();

      StringBuilder sb = null;

      for (int i = 0, n = input.length(); i < n; i++) {

        if (sb != null && leftCut[i]) {
          words.add(sb.toString());
          sb = null;
        }

        char c = input.charAt(i);
        if (isAlphabetic(c) || isDigit(c)) {

          if (sb == null) {
            sb = new StringBuilder();
          }
          sb.append(toLowerCase(c));

        } else if (sb != null) {
          words.add(sb.toString());
          sb = null;
        }
      }

      if (sb != null) {
        words.add(sb.toString());
      }

      return new NameStyled(words.stream().collect(toUnmodifiableList()));
    }
  }

  public String dashed() {
    return dashedOpt().orElse("");
  }

  public Optional<String> dashedOpt() {
    if (words.isEmpty()) {
      return Optional.empty();
    }
    return Optional.of(String.join("-", words));
  }

  private String firstUp(String str) {
    if (str == null) {
      return null;
    }
    if (str.isEmpty()) {
      return "";
    }
    return str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase();
  }

  public String camel() {
    return camelOpt().orElse("");
  }

  public Optional<String> camelOpt() {
    if (words.isEmpty()) {
      return Optional.empty();
    }
    if (words.size() == 1) {
      return Optional.of(words.get(0));
    }
    return Optional.of(words.get(0) + words.stream().skip(1).map(this::firstUp).collect(joining()));
  }

  public String CamelUp() {
    return CamelUpOpt().orElse("");
  }

  public Optional<String> CamelUpOpt() {
    if (words.isEmpty()) {
      return Optional.empty();
    }
    return Optional.of(words.stream().map(this::firstUp).collect(joining()));
  }

  public String UNDER_UP() {
    return UNDER_UP_OPT().orElse("");
  }

  public Optional<String> UNDER_UP_OPT() {
    if (words.isEmpty()) {
      return Optional.empty();
    }
    return Optional.of(String.join("_", words).toUpperCase());
  }

  public String dashedUp() {
    return dashedUpOpt().orElse("");
  }

  public Optional<String> dashedUpOpt() {
    if (words.isEmpty()) {
      return Optional.empty();
    }
    return Optional.of(String.join("-", words).toUpperCase());
  }

  public String under_lo() {
    return under_lo_opt().orElse("");
  }

  public Optional<String> under_lo_opt() {
    if (words.isEmpty()) {
      return Optional.empty();
    }
    return Optional.of(String.join("_", words));
  }

  @Override
  public String toString() {
    return "NameStyled[" + String.join("-", words) + ']';
  }

  public NameStyled concat(NameStyled nameStyled) {
    if (nameStyled == null || nameStyled.words.isEmpty()) {
      return this;
    }

    if (words.isEmpty()) {
      return nameStyled;
    }

    {
      List<String> list = new ArrayList<>(words.size() + nameStyled.words.size());
      list.addAll(words);
      list.addAll(nameStyled.words);
      return new NameStyled(list);
    }
  }
}
