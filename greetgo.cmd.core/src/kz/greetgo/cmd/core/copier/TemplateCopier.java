package kz.greetgo.cmd.core.copier;

import java.io.File;
import java.nio.file.Path;
import java.util.function.Consumer;
import kz.greetgo.cmd.core.copier.line_modifications.ModifierParser_password;
import kz.greetgo.cmd.core.copier.line_modifications.ModifierParser_port;
import kz.greetgo.cmd.core.copier.line_modifications.ModifierParser_replace;

public class TemplateCopier {
  private Path from;
  private Path to;

  private TemplateCopier() {}

  private final VariableStorage variableStorage = new VariableStorage();

  private Integer portBase;

  public static TemplateCopier of() {
    return new TemplateCopier();
  }

  public TemplateCopier from(Path from) {
    this.from = from;
    return this;
  }

  public TemplateCopier to(Path to) {
    this.to = to;
    return this;
  }

  public void copy() {
    CopyContext context = new CopyContext();
    context.variableStorage = variableStorage;
    context.modifierList.add(new ModifierParser_replace(variableStorage::replace));
    context.modifierList.add(new ModifierParser_password());

    if (portBase != null) {
      context.modifierList.add(new ModifierParser_port(portBase));
    }

    FileCopier root = FileCopier.root(from.toFile());
    root.modifierExt        = ".modifier.txt";
    root.checkIsFileTextual = this::isFileTextual;

    root.base       = to.toFile().getParentFile().toPath();
    root.targetName = to.toFile().getName();
    root.context    = context;

    root.fillChildren();

    root.readModifier();

    //    root.showYourself();

    root.apply();
  }

  private boolean isFileTextual(File file) {
    String name = file.getName().toLowerCase();

    return false
      || name.endsWith(".java")
      || name.endsWith(".kt")
      || name.endsWith(".js")
      || name.endsWith(".ts")
      || name.endsWith(".sh")
      || name.endsWith(".bash")
      || name.endsWith(".bat")

      || name.endsWith(".vue")

      || name.endsWith(".gradle")
      || name.endsWith(".txt")
      || name.endsWith(".xml")
      || name.endsWith(".md")
      || name.endsWith(".json")
      || name.endsWith(".html")

      || name.endsWith(".yaml")
      || name.endsWith(".yml")

      || name.endsWith(".css")
      || name.endsWith(".scss")
      || name.endsWith(".sass")
      || name.endsWith(".less")

      || name.endsWith(".jsp")
      || name.endsWith(".jspx")
      || name.endsWith(".asp")
      || name.endsWith(".aspx")

      || name.endsWith(".svg")
      || name.endsWith(".sql")

      || name.startsWith(".env.")
      || name.equals(".gitignore")
      ;
  }

  public TemplateCopier setVariable(String varName, String varValue) {
    variableStorage.put(varName, varValue);
    return this;
  }

  public TemplateCopier setVariable(String varName, NameStyled varValue) {
    variableStorage.put(varName, varValue);
    return this;
  }

  public TemplateCopier use(Consumer<TemplateCopier> consumer) {
    consumer.accept(this);
    return this;
  }

  public void setPortBase(Integer portBase) {
    this.portBase = portBase;
  }
}
