package kz.greetgo.cmd.core.copier.line_modifications;

import kz.greetgo.cmd.core.copier.LineHolder;

public interface Modifier {
  void modify(LineHolder lineHolder);
}
