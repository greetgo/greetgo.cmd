package kz.greetgo.cmd.core.copier.line_modifications;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import kz.greetgo.cmd.core.copier.LineHolder;

public class LineModificationBuilder {
  final String     command;
  final LineHolder commandLine;
  final Scope      scope;
  final String     name;
  final boolean    unpin;

  public LineModificationBuilder(Scope scope, String name, String command, LineHolder commandLine) {
    this.scope       = scope;
    this.name        = name;
    this.command     = command;
    this.commandLine = commandLine;
    unpin            = false;
  }

  public LineModificationBuilder(Scope scope, String name, boolean unpin, LineHolder commandLine) {
    this.scope       = scope;
    this.name        = name;
    this.unpin       = unpin;
    this.command     = null;
    this.commandLine = commandLine;
  }

  public static LineModificationBuilder tryParse(LineHolder line) {
    String cleanLine = clearLine(line.line);

    if (cleanLine == null) {
      return null;
    }

    Scope  scope = Scope.LINE;
    String name  = null;

    for (final String mod : new String[]{"///PIN ", "###PIN ", "---PIN "}) {
      if (cleanLine.startsWith(mod)) {

        var rest = cleanLine.substring(mod.length()).trim();
        var idx  = rest.indexOf(' ');
        if (idx < 0) {
          return null;
        }

        name      = rest.substring(0, idx);
        scope     = Scope.FILE;
        cleanLine = "///" + rest.substring(idx).trim();
        break;
      }
    }

    for (final String mod : new String[]{"///UNPIN ", "###UNPIN ", "---UNPIN "}) {
      if (cleanLine.startsWith(mod)) {

        name  = cleanLine.substring(mod.length()).trim();
        scope = Scope.FILE;

        return new LineModificationBuilder(scope, name, true, line);
      }
    }

    for (final String mod : new String[]{"///MODIFY ", "###MODIFY ", "---MODIFY "}) {
      if (cleanLine.startsWith(mod)) {
        return new LineModificationBuilder(scope, name, cleanLine.substring(mod.length()).trim(), line);
      }
    }

    return null;
  }

  public boolean isUnpin() {
    return unpin;
  }

  private static final String XML_START = "<!--";
  private static final String XML_END   = "-->";

  private static final String MD_START = "[comment]: <> (";
  private static final String MD_END   = ")";

  private static final Pattern JSON_COMMENT = Pattern.compile(
    "\\s*['\"]___comment_\\d*['\"]\\s*:\\s*['\"](.*)['\"]\\s*(,\\s*)?", Pattern.CASE_INSENSITIVE
  );

  public static String extractJsonComment(String line) {
    var matcher = JSON_COMMENT.matcher(line);
    if (!matcher.matches()) {
      return null;
    }
    return matcher.group(1);
  }

  private static String clearLine(String line) {
    if (line == null) {
      return null;
    }

    String trimmedLine = line.trim();

    if (trimmedLine.startsWith(XML_START) && trimmedLine.endsWith(XML_END)) {
      return trimmedLine.substring(XML_START.length(), trimmedLine.length() - XML_END.length()).trim();
    }
    if (trimmedLine.startsWith(MD_START) && trimmedLine.endsWith(MD_END)) {
      return trimmedLine.substring(MD_START.length(), trimmedLine.length() - MD_END.length()).trim();
    }

    {
      var extractedLine = extractJsonComment(line);
      if (extractedLine != null) {
        return extractedLine;
      }
    }

    return trimmedLine;
  }

  private final List<ModifierParser> parserList = new ArrayList<>();

  @SuppressWarnings("UnusedReturnValue")
  public LineModificationBuilder modifier(ModifierParser modifierParser) {
    parserList.add(modifierParser);
    return this;
  }

  public LineModification build() {
    return new LineModification(scope, name, command, commandLine, parserList);
  }

  public LineModificationBuilder modifiers(List<ModifierParser> modifierList) {
    modifierList.forEach(this::modifier);
    return this;
  }

  public String name() {
    return name;
  }
}
