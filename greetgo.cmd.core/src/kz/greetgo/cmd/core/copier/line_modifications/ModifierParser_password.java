package kz.greetgo.cmd.core.copier.line_modifications;

import java.security.SecureRandom;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import kz.greetgo.cmd.core.copier.LineHolder;
import kz.greetgo.cmd.core.util.RndUtil;

public class ModifierParser_password implements ModifierParser {

  private final ConcurrentHashMap<String, String> passwordMap = new ConcurrentHashMap<>();

  private final Random rnd = new SecureRandom();

  private String generate(String currentPassword) {
    return RndUtil.rndStr(currentPassword.length(), rnd);
  }

  @Override
  public Modifier parse(String command, LineHolder commandLine) {
    String[] split = command.split("\\s+");

    if (split.length != 2 || !"password".equals(split[0])) {
      return null;
    }

    String password = split[1];

    String replaceTo = passwordMap.computeIfAbsent(password, this::generate);

    return line -> line.modifingLine = line.modifingLine.replaceAll(password, "" + replaceTo);

  }
}
