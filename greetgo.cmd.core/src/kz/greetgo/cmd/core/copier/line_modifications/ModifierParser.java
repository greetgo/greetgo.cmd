package kz.greetgo.cmd.core.copier.line_modifications;

import kz.greetgo.cmd.core.copier.LineHolder;

public interface ModifierParser {
  /**
   * Парсит команду возвращает подготовленный для модификации объект.
   * Если команда не относиться к данному анализатору, то возвращается null
   *
   * @param command текст команды
   * @param commandLine
   * @return сгенерированный модификатор, или null, если команда другая
   */
  Modifier parse(String command, LineHolder commandLine);
}
