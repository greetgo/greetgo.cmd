package kz.greetgo.cmd.core.copier.line_modifications;

import java.util.List;
import kz.greetgo.cmd.core.copier.LineHolder;

public class LineModification {
  private final String               command;
  private final LineHolder           commandLine;
  private final List<ModifierParser> parserList;
  private final Scope                scope;
  private final String               name;

  public LineModification(Scope scope, String name,
                          String command, LineHolder commandLine,
                          List<ModifierParser> parserList) {

    this.scope       = scope;
    this.name        = name;
    this.command     = command;
    this.commandLine = commandLine;
    this.parserList  = parserList;
  }

  public Scope scope() {
    return scope;
  }

  public void modify(LineHolder line) {

    for (final ModifierParser modifierParser : parserList) {
      Modifier modifier = modifierParser.parse(command, commandLine);
      if (modifier != null) {
        modifier.modify(line);
        return;
      }
    }

    throw new RuntimeException("g6UAuSo8JR :: Unknown modify command: `" + command + "'" +
                                 " at line " + commandLine.lineNo + " in " + commandLine.file);
  }

  public String name() {
    return name;
  }
}
