package kz.greetgo.cmd.core.copier.line_modifications;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import kz.greetgo.cmd.core.copier.LineHolder;

public class ModifierParser_port implements ModifierParser {

  private final AtomicInteger portSequence = new AtomicInteger();

  private final ConcurrentHashMap<String, Integer> portMap = new ConcurrentHashMap<>();

  public ModifierParser_port(int portBase) {
    portSequence.set(portBase);
  }

  @Override
  public Modifier parse(String command, LineHolder commandLine) {
    String[] split = command.split("\\s+");

    if (split.length != 2 || !"port".equals(split[0])) {
      return null;
    }

    String port = split[1];

    int replaceTo = portMap.computeIfAbsent(port, p -> portSequence.getAndIncrement());

    return line -> line.modifingLine = line.modifingLine.replaceAll(port, "" + replaceTo);

  }
}
