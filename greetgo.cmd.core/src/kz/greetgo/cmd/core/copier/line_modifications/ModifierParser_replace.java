package kz.greetgo.cmd.core.copier.line_modifications;

import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kz.greetgo.cmd.core.copier.LineHolder;

public class ModifierParser_replace implements ModifierParser {

  private final Function<String, String> formulaEval;

  public ModifierParser_replace(Function<String, String> formulaEval) {
    this.formulaEval = formulaEval;
  }

  @Override
  public Modifier parse(String command, LineHolder commandLine) {
    String[] split = command.split("\\s+");
    if (split.length == 3 && split[0].equals("replace")) {
      String findRegexp          = split[1];
      String replaceValueFormula = split[2];

      return line -> {
        String        replacement = formulaEval.apply(replaceValueFormula);
        Pattern       regex           = Pattern.compile(findRegexp);
        StringBuilder result      = new StringBuilder();
        Matcher       matcher     = regex.matcher(line.modifingLine);
        while (matcher.find()) {
          matcher.appendReplacement(result, replacement);
        }
        matcher.appendTail(result);

        line.modifingLine = result.toString();
      };
    }

    return null;
  }
}
