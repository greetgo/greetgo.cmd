package kz.greetgo.cmd.core.copier.line_modifications;

public enum Scope {
  LINE, FILE
}
