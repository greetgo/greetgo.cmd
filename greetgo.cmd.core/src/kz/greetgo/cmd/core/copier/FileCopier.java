package kz.greetgo.cmd.core.copier;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.PosixFilePermission;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import kz.greetgo.cmd.core.copier.file_modifications.DestinationOperation;
import kz.greetgo.cmd.core.copier.line_modifications.LineModification;
import kz.greetgo.cmd.core.copier.line_modifications.LineModificationBuilder;
import kz.greetgo.cmd.core.util.StrUtil;
import lombok.SneakyThrows;

import static java.nio.charset.StandardCharsets.UTF_8;
import static kz.greetgo.cmd.core.util.StrUtil.spaces;

public class FileCopier {

  public final FileCopier parent;
  public final File       fromFile;
  public final int        level;
  public       String     targetName;

  public final List<FileCopier> children = new ArrayList<>();
  public       Path             base;
  public       CopyContext      context  = null;

  private FileCopier(FileCopier parent, File fromFile, int level) {
    this.parent   = parent;
    this.fromFile = fromFile;
    targetName    = fromFile.getName();
    this.level    = level;
  }

  public CopyContext context() {
    if (context != null) {
      return context;
    }
    if (parent == null) {
      throw new RuntimeException("q601sDP3PC :: parent == null");
    }
    return parent.context();
  }

  public VariableStorage variableStorage() {
    return context().variableStorage;
  }

  public String modifierExt = null;

  public static FileCopier of(FileCopier parent, File fromFile, int level) {
    return new FileCopier(parent, fromFile, level);
  }

  public static FileCopier root(File fromFile) {
    return of(null, fromFile, 0);
  }

  String newLine = "\n";

  public void apply() {
    if (skip) {
      return;
    }

    applyOnlyMine();

    children.forEach(FileCopier::apply);
  }

  private void applyOnlyMine() {
    if (!fromFile.isFile()) {
      return;
    }

    try {

      if (isBinary()) {

        byte[] bytes = Files.readAllBytes(fromFile.toPath());

        writeToDestination(bytes);

      } else {

        List<String> lines = Files.readAllLines(fromFile.toPath(), UTF_8);

        lines = modify(lines);

        if (lines.size() > 0) {
          var last = lines.get(lines.size() - 1);
          if (last.length() > 0) {
            lines.add("");
          }
        }

        byte[] bytes = lines.stream().collect(Collectors.joining(newLine)).getBytes(UTF_8);

        writeToDestination(bytes);

      }

    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private List<String> modify(List<String> lines) {
    List<String> ret = new ArrayList<>();

    List<LineModification>                  lineModifiers = new ArrayList<>();
    LinkedHashMap<String, LineModification> fileModifiers = new LinkedHashMap<>();

    int lineNo = 0;

    for (String line : lines) {
      lineNo++;

      var lineHolder = new LineHolder(line, lineNo, fromFile);

      LineModificationBuilder builder = LineModificationBuilder.tryParse(lineHolder);

      if (builder != null) {
        if (builder.isUnpin()) {
          fileModifiers.remove(builder.name());
          continue;
        }

        LineModification lm = builder.modifiers(context().modifierList).build();
        switch (lm.scope()) {
          case LINE:
            lineModifiers.add(lm);
            continue;

          case FILE:
            fileModifiers.put(lm.name(), lm);
            continue;

          default:
            throw new RuntimeException("i0NuVxxTOZ :: Unknown scope " + lm.scope());
        }
      }

      for (LineModification cmd : lineModifiers) {
        cmd.modify(lineHolder);
      }
      for (LineModification cmd : fileModifiers.values()) {
        cmd.modify(lineHolder);
      }

      ret.add(lineHolder.modifingLine);
      lineModifiers.clear();
    }

    return ret;
  }


  private void writeToDestination(byte[] bytes) throws IOException {
    Path destinationPath = destinationPath();
    destinationPath.toFile().getParentFile().mkdirs();
    Files.write(destinationPath, bytes);
    for (final DestinationOperation destinationOperation : destinationOperationList) {
      destinationOperation.execute(destinationPath, context());
    }
  }

  private final List<DestinationOperation> destinationOperationList = new ArrayList<>();

  BinStatus binStatus = BinStatus.AUTO;

  public Predicate<File> checkIsFileTextual = null;

  public Predicate<File> getCheckIsFileTextual() {
    {
      Predicate<File> local = checkIsFileTextual;
      if (local != null) {
        return local;
      }
    }

    return checkIsFileTextual = getLiveParent().getCheckIsFileTextual();
  }

  private boolean isBinary() {
    switch (binStatus) {
      case BIN:
        return true;
      case TXT:
        return false;
    }

    return !getCheckIsFileTextual().test(fromFile);
  }

  public String getModifierExt() {
    {
      String localConfExt = modifierExt;
      if (localConfExt != null) {
        return localConfExt;
      }
    }

    return modifierExt = getLiveParent().getModifierExt();
  }

  boolean modifierWasRead = false;

  public void readModifier() {
    if (modifierWasRead) {
      return;
    }
    modifierWasRead = true;

    doReadModifier();

    children.forEach(FileCopier::readModifier);
  }

  private boolean skip = false;

  @SneakyThrows
  private void doReadModifier() {
    File modifierFile = new File(fromFile.getPath() + getModifierExt());

    if (!modifierFile.exists()) {
      return;
    }

    List<String> lines = Files.readAllLines(modifierFile.toPath());

    for (String line : lines) {
      if (line.trim().isEmpty()) {
        continue;
      }
      if (line.trim().startsWith("#")) {
        continue;
      }

      int eqIdx = line.indexOf('=');
      if (eqIdx < 0) {

        if ("make-exec".equals(line.trim())) {
          destinationOperationList.add((path, context) -> {
            try {
              Files.setPosixFilePermissions(path, Set.of(
                PosixFilePermission.OWNER_EXECUTE,
                PosixFilePermission.GROUP_EXECUTE,
                PosixFilePermission.OTHERS_EXECUTE,

                PosixFilePermission.OWNER_READ,
                PosixFilePermission.GROUP_READ,
                PosixFilePermission.OTHERS_READ,

                PosixFilePermission.OWNER_WRITE,
                PosixFilePermission.GROUP_WRITE
              ));
            } catch (IOException e) {
              throw new RuntimeException(e);
            }
          });
        }

        continue;
      }

      String key   = line.substring(0, eqIdx).trim();
      String value = line.substring(eqIdx + 1);

      if ("skip".equals(key)) {
        skip = StrUtil.toBool(value);
        continue;
      }

      if ("rename-to".equals(key)) {
        renameTo(value);
        continue;
      }

      if ("bin-status".equals(key)) {
        binStatus = BinStatus.valueOf(value.toUpperCase());
        continue;
      }

      throw new RuntimeException("rg8zv8Ou8K :: Unknown modifier parameter: " + key + " = " + value);
    }
  }

  private void renameTo(String newName) {
    targetName = variableStorage().replace(newName.trim());
  }

  private FileCopier getLiveParent() {
    FileCopier localParent = parent;
    if (localParent == null) {
      throw new RuntimeException("parent is null");
    }
    return localParent;
  }

  public FileCopier fillChildren() {
    if (!fromFile.isDirectory()) {
      return this;
    }

    File[] files = fromFile.listFiles();

    if (files == null) {
      return this;
    }

    for (File file : files) {
      if (file.getName().endsWith(getModifierExt())) {
        continue;
      }

      children.add(FileCopier.of(this, file, level + 1).fillChildren());
    }

    return this;
  }

  @SuppressWarnings("unused")
  public void showYourself() {
    String        end = fromFile.isDirectory() ? "/" : "";
    StringBuilder pr  = new StringBuilder(spaces(level * 2) + fromFile.getName() + end);
    for (int i = pr.length(); i < 50; i++) {
      pr.append(i % 3 == 0 ? '.' : ' ');
    }
    System.out.println(pr + " " + destinationPath() + end);
    children.forEach(FileCopier::showYourself);
  }

  private Path destinationPath() {
    if (base != null) {
      return base.resolve(targetName);
    }

    if (parent == null) {
      throw new RuntimeException("parent == null");
    }

    return parent.destinationPath().resolve(targetName);
  }

}
