package kz.greetgo.cmd.core.copier;

import java.io.File;
import lombok.SneakyThrows;

public class LineHolder {
  public final String line;
  public final int    lineNo;
  public final File   file;
  public       String modifingLine;

  public LineHolder(String line, int lineNo, File file) {
    this.line   = line;
    this.lineNo = lineNo;
    this.file   = file;

    modifingLine = line;
  }

  @SneakyThrows
  public static LineHolder of(String line) {
    return new LineHolder(line, 1, File.createTempFile("line", ".txt"));
  }
}
