package kz.greetgo.cmd.core.copier.file_modifications;

import java.nio.file.Path;
import kz.greetgo.cmd.core.copier.CopyContext;

public interface DestinationOperation {
  void execute(Path path, CopyContext context);
}
