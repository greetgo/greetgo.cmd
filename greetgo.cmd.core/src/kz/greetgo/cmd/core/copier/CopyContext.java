package kz.greetgo.cmd.core.copier;

import java.util.ArrayList;
import java.util.List;
import kz.greetgo.cmd.core.copier.line_modifications.ModifierParser;

public class CopyContext {
  public VariableStorage variableStorage;

  public final List<ModifierParser> modifierList = new ArrayList<>();

}
