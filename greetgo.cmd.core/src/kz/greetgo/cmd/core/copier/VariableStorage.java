package kz.greetgo.cmd.core.copier;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class VariableStorage {

  private final Map<String, Object> map = new HashMap<>();

  public String replace(String value) {
    if (value == null) {
      return null;
    }

    StringBuilder sb = new StringBuilder();

    int index = 0;

    int valueLength = value.length();

    while (index < valueLength) {

      int i1 = value.indexOf('{', index);
      if (i1 < 0) {
        break;
      }

      if (i1 > 0 && '\\' == value.charAt(i1 - 1)) {
        sb.append(value, index, i1 - 1).append('{');
        index = i1 + 1;
        continue;
      }

      int i2 = value.indexOf('}', i1);
      if (i2 < 0) {
        break;
      }

      sb.append(value, index, i1);
      get(value.substring(i1 + 1, i2))
        .ifPresentOrElse(sb::append, () -> sb.append(value, i1, i2 + 1));

      index = i2 + 1;
    }

    sb.append(value.substring(index));

    return sb.toString();
  }

  public void put(String varName, String varValue) {
    map.put(varName, varValue);
  }

  public void put(String varName, NameStyled varValue) {
    map.put(varName, varValue);
  }

  public Optional<String> get(String formula) {

    var split = formula.split("\\.");

    if (split.length == 1) {

      Object object = map.get(formula);

      if (object == null) {
        return Optional.empty();
      }

      if (object instanceof String) {
        return Optional.of((String) object);
      }

      if (object instanceof NameStyled) {
        return Optional.of(((NameStyled) object).dashed());
      }

      throw new RuntimeException("IOtnV020j9 :: Unknown value class."
                                   + " Key = " + formula
                                   + ", Value.Class = " + object.getClass());
    }

    if (split.length == 2) {

      Object object = map.get(split[0]);
      String func   = split[1];

      NameStyled nameStyled;
      if (object instanceof NameStyled) {
        nameStyled = (NameStyled) object;
      } else if (object instanceof String) {
        nameStyled = NameStyled.parse((String) object);
      } else {
        throw new RuntimeException("0mrK7Y7Q9f :: Unknown value for formula " + formula + " :: Value = " + object);
      }

      switch (func) {
        case "camel":
          return nameStyled.camelOpt();
        case "Camel":
          return nameStyled.CamelUpOpt();
        case "UNDER":
          return nameStyled.UNDER_UP_OPT();
        case "under":
          return nameStyled.under_lo_opt();
        case "DASHED":
          return nameStyled.dashedUpOpt();
        case "dashed":
          return nameStyled.dashedOpt();
      }

      throw new RuntimeException("3LeaXf8pP2 :: Unknown function in formula : " + formula);
    }

    throw new RuntimeException("YGsNSp4R2t :: Unknown formula format: " + formula);
  }
}
