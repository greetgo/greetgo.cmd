package kz.greetgo.cmd.core.errors;

import java.util.List;
import lombok.NonNull;

public class LeftExitCodeOfCommand extends RuntimeException {
  public LeftExitCodeOfCommand(int exitCode, String cmd, List<String> stdErr) {
    super(message(exitCode, cmd, stdErr));
  }

  private static @NonNull String message(int exitCode, String cmd, List<String> stdErr) {
    StringBuilder sb = new StringBuilder();
    sb.append("Error exit code ").append(exitCode).append(" of executed command `").append(cmd).append("'");
    for (final String errLine : stdErr) {
      sb.append('\n').append(errLine);
    }
    return sb.toString();
  }
}
