package kz.greetgo.cmd.core.copier;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.fest.assertions.api.Assertions.assertThat;

public class NameStyledTest {

  @DataProvider
  private Object[][] parseDataProvider() {
    return new Object[][]{
      {"HelloWorldPeople", "hello-world-people"},
      {"STATUS_OF_THE_WORLD", "status-of-the-world"},
      {"strausDownUp", "straus-down-up"},
      {"hot_water_rise", "hot-water-rise"},
      {"the stone is gone away", "the-stone-is-gone-away"},
      {"TResult", "t-result"},
      {"STONEResult", "stone-result"},
      {"BeginOfSTONEResult", "begin-of-stone-result"},
      {"BeginOfSTONE7RED117Result", "begin-of-stone7-red117-result"},
      {"Hello67Fox21", "hello67-fox21"},
      {"31helloWorldData", "31-hello-world-data"},
      {"PointX", "point-x"},
      {"PointNANO", "point-nano"},
      {"STPointNANO", "st-point-nano"},
      {"WORLDPointToSTEEL", "world-point-to-steel"},
      {"asd123wow88boom", "asd123-wow88-boom"},
      {"ASD123WOW88BOOM", "asd123-wow88-boom"},
      {"RED81", "red81"},
    };
  }

  @Test(dataProvider = "parseDataProvider")
  public void parse(String input, String dashedVariant) {

    //
    //
    NameStyled nameStyled = NameStyled.parse(input);
    //
    //

    assertThat(String.join("-", nameStyled.words)).isEqualTo(dashedVariant);
  }

  @Test
  public void dashed() {
    NameStyled nameStyled = NameStyled.parse("sinus-stone-hello");

    //
    //
    var actual = nameStyled.dashed();
    //
    //

    assertThat(actual).isEqualTo("sinus-stone-hello");
  }

  @Test
  public void dashedUp() {
    NameStyled nameStyled = NameStyled.parse("sinus-stone-hello");

    //
    //
    var actual = nameStyled.dashedUp();
    //
    //

    assertThat(actual).isEqualTo("SINUS-STONE-HELLO");
  }

  @Test
  public void camel() {
    NameStyled nameStyled = NameStyled.parse("sinus-stone-hello");

    //
    //
    var actual = nameStyled.camel();
    //
    //

    assertThat(actual).isEqualTo("sinusStoneHello");
  }

  @Test
  public void camelUp() {
    NameStyled nameStyled = NameStyled.parse("sinus-stone-hello");

    //
    //
    var actual = nameStyled.CamelUp();
    //
    //

    assertThat(actual).isEqualTo("SinusStoneHello");
  }

  @Test
  public void UNDER_UP() {
    NameStyled nameStyled = NameStyled.parse("sinus-stone-hello");

    //
    //
    var actual = nameStyled.UNDER_UP();
    //
    //

    assertThat(actual).isEqualTo("SINUS_STONE_HELLO");
  }

  @Test
  public void under_lo() {
    NameStyled nameStyled = NameStyled.parse("sinus-stone-hello");

    //
    //
    var actual = nameStyled.under_lo();
    //
    //

    assertThat(actual).isEqualTo("sinus_stone_hello");
  }
}
