package kz.greetgo.cmd.core.copier;

import java.util.Arrays;
import java.util.stream.Stream;
import kz.greetgo.util.RND;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.fest.assertions.api.Assertions.assertThat;

public class VariableStorageTest {

  private VariableStorage storage;

  @BeforeMethod
  public void storage___new_VariableStorage() {
    storage = new VariableStorage();
  }

  @DataProvider
  private Object[][] replace__DataProvider() {
    var arr = new String[][]{
      //@formatter:off
      {  "Hello {NAME} world"       ,  "space-deep-relaxation", "Hello space-deep-relaxation world"  },
      {  "Hello {NAME.dashed} world",  "space-deep-relaxation", "Hello space-deep-relaxation world"  },
      {  "Hello {NAME.DASHED} world",  "space-deep-relaxation", "Hello SPACE-DEEP-RELAXATION world"  },
      {  "Hello {NAME.UNDER} world" ,  "space-deep-relaxation", "Hello SPACE_DEEP_RELAXATION world"  },
      {  "Hello {NAME.under} world" ,  "space-deep-relaxation", "Hello space_deep_relaxation world"  },
      {  "Hello {NAME.camel} world" ,  "space-deep-relaxation", "Hello spaceDeepRelaxation world"    },
      {  "Hello {NAME.Camel} world" ,  "space-deep-relaxation", "Hello SpaceDeepRelaxation world"    },

      {  "{NAME}",  "space-deep-relaxation", "space-deep-relaxation"  },

      {  "{NAME.camel} {NAME.UNDER}",  "space-deep-relaxation", "spaceDeepRelaxation SPACE_DEEP_RELAXATION"  },
      //@formatter:on
    };

    return Arrays.stream(arr).flatMap(x -> Stream.of(
      new Object[]{x[0], x[1], x[2], false},
      new Object[]{x[0], x[1], x[2], true}
    )).toArray(Object[][]::new);
  }

  @Test(dataProvider = "replace__DataProvider")
  public void replace(String startText, String varValue, String expectedResult, boolean useNameStyled) {

    if (useNameStyled) {
      storage.put("NAME", NameStyled.parse(varValue));
    } else {
      storage.put("NAME", varValue);
    }

    //
    //
    var result = storage.replace(startText);
    //
    //

    assertThat(result).isEqualTo(expectedResult);
  }


  @Test
  public void replace__differentCases_1() {

    storage.put("NAME", NameStyled.parse("big-game"));
    storage.put("name", NameStyled.parse("small-game"));

    //
    //
    var result = storage.replace("The {NAME.Camel} and the {name.Camel}.");
    //
    //

    assertThat(result).isEqualTo("The BigGame and the SmallGame.");
  }

  @Test
  public void replace__differentCases_2() {

    storage.put("NAME", "Big-Game");
    storage.put("name", "Small__Game");

    //
    //
    var result = storage.replace("The {NAME} and the {name}.");
    //
    //

    assertThat(result).isEqualTo("The Big-Game and the Small__Game.");
  }


  @Test
  public void replace__noVars_1() {

    //
    //
    var result = storage.replace("Some any string");
    //
    //

    assertThat(result).isEqualTo("Some any string");
  }

  @Test
  public void replace__noVars_2() {

    //
    //
    var result = storage.replace("Some any { string");
    //
    //

    assertThat(result).isEqualTo("Some any { string");
  }

  @Test
  public void replace__withPrefix() {

    //
    //
    var result = storage.replace("Some any \\{NAME} string");
    //
    //

    assertThat(result).isEqualTo("Some any {NAME} string");
  }

  @Test
  public void replace__withPrefixAndVar() {

    storage.put("NAME", "Samara");

    //
    //
    var result = storage.replace("Some any \\{NAME} string + {NAME} HI");
    //
    //

    assertThat(result).isEqualTo("Some any {NAME} string + Samara HI");
  }

  @Test
  public void replace__noVar() {

    storage.put("NAME1", "Samara");

    //
    //
    var result = storage.replace("Some any {NAME} HI");
    //
    //

    assertThat(result).isEqualTo("Some any {NAME} HI");
  }

  @Test
  public void replace__nakedVar() {

    storage.put("NAME", "Samara");

    //
    //
    var result = storage.replace("{NAME}");
    //
    //

    assertThat(result).isEqualTo("Samara");
  }

  @Test
  public void replace__nakedVar__NameStyled() {

    storage.put("NAME", NameStyled.parse("Samara-is-rising"));

    //
    //
    var result = storage.replace("{NAME.UNDER}");
    //
    //

    assertThat(result).isEqualTo("SAMARA_IS_RISING");
  }

  @DataProvider
  private Object[][] get__DataProvider() {
    var arr = new String[][]{
      //@formatter:off
      {  "NAME.dashed", "space-deep-relaxation", "space-deep-relaxation"  },
      {  "NAME.DASHED", "space-deep-relaxation", "SPACE-DEEP-RELAXATION"  },
      {  "NAME.UNDER",  "space-deep-relaxation", "SPACE_DEEP_RELAXATION"  },
      {  "NAME.under",  "space-deep-relaxation", "space_deep_relaxation"  },
      {  "NAME.camel",  "space-deep-relaxation", "spaceDeepRelaxation"    },
      {  "NAME.Camel",  "space-deep-relaxation", "SpaceDeepRelaxation"    },

      {  "NAME"      ,  "space-deep-relaxation", "space-deep-relaxation"   },
      //@formatter:on
    };

    return Arrays.stream(arr).flatMap(x -> Stream.of(
      new Object[]{x[0], x[1], x[2], false},
      new Object[]{x[0], x[1], x[2], true}
    )).toArray(Object[][]::new);
  }

  @Test(dataProvider = "get__DataProvider")
  public void get(String formula, String sourceValue, String expectedResult, boolean useNameStyled) {
    if (useNameStyled) {
      storage.put("NAME", NameStyled.parse(sourceValue));
    } else {
      storage.put("NAME", sourceValue);
    }

    //
    //
    var result = storage.get(formula);
    //
    //

    assertThat(result.orElseThrow()).isEqualTo(expectedResult);

  }

  @Test
  public void get__COPY() {
    String text = "hj1v4535-j4JJdJF__32134jf  fds 54   fd111fs";

    storage.put("NAME", text);

    //
    //
    var result = storage.get("NAME");
    //
    //

    assertThat(result.orElseThrow()).isEqualTo(text);
  }

  @Test
  public void get__NoValue() {
    //
    //
    var result = storage.get("NO_VAL");
    //
    //

    var rnd = RND.str(10);

    assertThat(result.orElse(rnd)).isEqualTo(rnd);
  }
}
