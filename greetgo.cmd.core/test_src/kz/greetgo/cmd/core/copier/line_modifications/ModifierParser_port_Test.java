package kz.greetgo.cmd.core.copier.line_modifications;

import kz.greetgo.cmd.core.copier.LineHolder;
import org.testng.annotations.Test;

import static org.fest.assertions.api.Assertions.assertThat;

public class ModifierParser_port_Test {

  @Test
  public void parse__modify() {

    var parser = new ModifierParser_port(1000);

    {
      //
      //
      var modifier = parser.parse("port 123", LineHolder.of(""));
      //
      //

      var line1 = LineHolder.of("some text 123 wow 123 hello world");
      var line2 = LineHolder.of("hello world 123 goto 123 Saratov 123 town down");

      //
      //
      modifier.modify(line1);
      modifier.modify(line2);
      //
      //

      assertThat(line1.modifingLine).isEqualTo("some text 1000 wow 1000 hello world");
      assertThat(line2.modifingLine).isEqualTo("hello world 1000 goto 1000 Saratov 1000 town down");
    }
    {
      //
      //
      var modifier = parser.parse("port 3111", LineHolder.of(""));
      //
      //

      var line1 = LineHolder.of("some text 3111 wow 3111 hello world");
      var line2 = LineHolder.of("hello world 3111 goto 3111 Saratov 3111 town down");

      //
      //
      modifier.modify(line1);
      modifier.modify(line2);
      //
      //

      assertThat(line1.modifingLine).isEqualTo("some text 1001 wow 1001 hello world");
      assertThat(line2.modifingLine).isEqualTo("hello world 1001 goto 1001 Saratov 1001 town down");
    }

  }
}
