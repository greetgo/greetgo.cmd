package kz.greetgo.cmd.core.copier.line_modifications;

import kz.greetgo.cmd.core.copier.LineHolder;
import org.testng.annotations.Test;

import static org.fest.assertions.api.Assertions.assertThat;

public class LineModificationBuilderTest {

  @Test
  public void tryParse__noCommand() {

    //
    //
    var builder = LineModificationBuilder.tryParse(LineHolder.of("left"));
    //
    //

    assertThat(builder).isNull();
  }

  @Test
  public void tryParse__MODIFY() {

    //
    //
    var builder = LineModificationBuilder.tryParse(LineHolder.of("  ///MODIFY replace test1 test2"));
    //
    //

    assertThat(builder).isNotNull();
    assert builder != null;
    assertThat(builder.isUnpin()).isFalse();
    assertThat(builder.command).isEqualTo("replace test1 test2");
    assertThat(builder.scope).isEqualTo(Scope.LINE);
    assertThat(builder.name).isNull();
  }

  @Test
  public void tryParse__PIN_MODIFY() {

    //
    //
    var builder = LineModificationBuilder.tryParse(LineHolder.of("  ///PIN m234 MODIFY replace test1 test2"));
    //
    //

    assertThat(builder).isNotNull();
    assert builder != null;
    assertThat(builder.isUnpin()).isFalse();
    assertThat(builder.command).isEqualTo("replace test1 test2");
    assertThat(builder.scope).isEqualTo(Scope.FILE);
    assertThat(builder.name).isEqualTo("m234");
  }

  @Test
  public void tryParse__UNPIN() {

    //
    //
    var builder = LineModificationBuilder.tryParse(LineHolder.of("  ///UNPIN m43215"));
    //
    //

    assertThat(builder).isNotNull();
    assert builder != null;
    assertThat(builder.isUnpin()).isTrue();
    assertThat(builder.name).isEqualTo("m43215");
  }

  @Test
  public void extractJsonComment__ok() {

    String line = " '___comment_43214': '###MODIFY replace status {status}',  ".replace('\'', '"');

    //
    //
    var result = LineModificationBuilder.extractJsonComment(line);
    //
    //

    assertThat(result).isEqualTo("###MODIFY replace status {status}");
  }

  @Test
  public void extractJsonComment__no() {

    String line = " Hello World ";

    //
    //
    var result = LineModificationBuilder.extractJsonComment(line);
    //
    //

    assertThat(result).isNull();
  }

}
